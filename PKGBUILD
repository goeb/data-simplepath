# Maintainer: Stefan Göbel <data-simplepath —at— subtype —dot— de>

pkgname='perl-data-simplepath'
_module='Data-SimplePath'
pkgver='0.005'
pkgrel='3'
arch=('any')
pkgdesc='Path-like access to complex data structures.'
url="https://metacpan.org/release/$_module"
license=('PerlArtistic' 'GPL')
options=('!emptydirs')
checkdepends=('perl-test-classapi' 'perl-test-nowarnings' 'perl-test-warn')

_pkgbuild_dir=${_pkgbuild_dir:-$PWD}
_source_files=(
   'lib'
   't'
   'Changes'
   'Makefile.PL'
   'MANIFEST'
   'MANIFEST.SKIP'
   'PKGBUILD'
   'README'
)

prepare() {

   local _dest="$srcdir/$pkgname"
   local _file=''

   mkdir -p "$_dest"

   for _file in "${_source_files[@]}" ; do
      cp -avx "$_pkgbuild_dir/$_file" "$_dest/"
   done

}

build() {
   (
      unset PERL5LIB PERL_MM_OPT PERL_LOCAL_LIB_ROOT
      export PERL_MM_USE_DEFAULT='1' PERL_AUTOINSTALL='--skipdeps'
      cd "$srcdir/$pkgname"
      perl Makefile.PL INSTALLDIRS='vendor'
      make
   )
}

check() {
   (
      cd "$srcdir/$pkgname"
      unset PERL5LIB PERL_MM_OPT PERL_LOCAL_LIB_ROOT
      export PERL_MM_USE_DEFAULT='1'
      make test
   )
}

package() {
   (
      cd "$srcdir/$pkgname"
      unset PERL5LIB PERL_MM_OPT PERL_LOCAL_LIB_ROOT
      make install INSTALLDIRS='vendor' DESTDIR="$pkgdir"
      find "$pkgdir" -name .packlist     -delete
      find "$pkgdir" -name perllocal.pod -delete
      find "$pkgdir" -type d -empty      -delete
   )
}

#:indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=87: